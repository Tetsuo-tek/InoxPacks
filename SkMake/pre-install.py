#!/usr/bin/env python

###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import os
import pytz

from PySketch.log           import msg, dbg, wrn, err, cri, printPair
from PySketch.fs            import *

from PCK.props              import *
from PCK.procs              import *

def main(env):
    baseSystem = env["baseSystem"]
    host = env["host"]
    pck = env["pck"]

    binary = os.path.join(baseSystem._binPath, "skmake")

    if pathExists(binary):
        wrn("Skipping standard Makefile compilation (skmake is ALREADY in the system)")
        return True

    wrn("Building 'SkMake' (first time) ..")

    specialkSrcPath = "{}.pck/SpecialK".format(os.path.join(baseSystem._packsInstallPath, "SpecialK"))
    destLink = "{}".format(os.path.join(pck._installPath, "SpecialK"))

    if not symLink(specialkSrcPath, destLink):
        return False
        
    skmakePath = os.path.join(pck._installPath, "SkMake")

    if not changeDir(skmakePath):
        return False

    makefileDist = None
    
    #host = HostMachine()
    opSys = host.infos()["os"]

    if not opSys in baseSystem._supportedOS:
        err("Operating System UNKONWN: {}".format(opSys))
        return False

    if opSys == "Linux":
        makefileDist = "Makefile-linux.dist"

    elif opSys == "FreeBSD":
        makefileDist = "Makefile-freebsd.dist"

    elif opSys == "Darwin":
        makefileDist = "Makefile-brew.dist"        

    dbg("'SkMake' Makefile selected: {}".format(makefileDist))

    if not copyFile(makefileDist, "Makefile"):
        return False

    cores = host.infos()["cpu-cores"]

    if not make("clean") or not make("-j{}".format(cores)):
        return False

    #if not rename(srcFileName, renamed):
    #    return False
    src = "SkMake.bin"
    installName = "skmake"

    target = os.path.join(baseSystem._binPath, installName) 

    if not rename(os.path.abspath(src), target):
        return False
    
    ok = (
        writeRuntimeDateTime(pck._installPath, "BUILT", datetime.now(pytz.UTC)) and
        writeRuntimeJson(pck._installPath, "ENTRY", ["{}".format(target)])
    )

    if ok:
        dbg("'SkMake' binary INSTALLED: {}".format(installName))

    return ok
